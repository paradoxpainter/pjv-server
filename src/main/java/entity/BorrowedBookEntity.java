package entity;

import javax.persistence.*;
import java.sql.Date;
import java.util.Objects;

@Entity
@Table(name = "borrowed_book", schema = "public", catalog = "db19_shcheden")
public class BorrowedBookEntity {
    private int borrowId;
    private int bookId;
    private int clientId;
    private Date borrowDt;
    private Date returnDt;
    private BookEntity bookByBookId;
    private ClientEntity clientByClientId;

    @Id
    @Column(name = "borrow_id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int getBorrowId() {
        return borrowId;
    }

    public void setBorrowId(int borrowId) {
        this.borrowId = borrowId;
    }

    @Basic
    @Column(name = "book_id", nullable = false)
    public int getBookId() {
        return bookId;
    }

    public void setBookId(int bookId) {
        this.bookId = bookId;
    }

    @Basic
    @Column(name = "client_id", nullable = false)
    public int getClientId() {
        return clientId;
    }

    public void setClientId(int clientId) {
        this.clientId = clientId;
    }

    @Basic
    @Column(name = "borrow_dt", nullable = false)
    public Date getBorrowDt() {
        return borrowDt;
    }

    public void setBorrowDt(Date borrowDt) {
        this.borrowDt = borrowDt;
    }

    @Basic
    @Column(name = "return_dt", nullable = true)
    public Date getReturnDt() {
        return returnDt;
    }

    public void setReturnDt(Date returnDt) {
        this.returnDt = returnDt;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BorrowedBookEntity that = (BorrowedBookEntity) o;
        return borrowId == that.borrowId &&
                bookId == that.bookId &&
                clientId == that.clientId &&
                Objects.equals(borrowDt, that.borrowDt) &&
                Objects.equals(returnDt, that.returnDt);
    }

    @Override
    public int hashCode() {
        return Objects.hash(borrowId, bookId, clientId, borrowDt, returnDt);
    }

    @ManyToOne(fetch = FetchType.LAZY) // here
    @JoinColumn(name = "book_id", referencedColumnName = "book_id", nullable = false, insertable = false, updatable = false)
    public BookEntity getBookByBookId() {
        return bookByBookId;
    }

    public void setBookByBookId(BookEntity bookByBookId) {
        this.bookByBookId = bookByBookId;
    }

    @ManyToOne(fetch = FetchType.LAZY) // here
    @JoinColumn(name = "client_id", referencedColumnName = "client_id", nullable = false, insertable = false, updatable = false)
    public ClientEntity getClientByClientId() {
        return clientByClientId;
    }

    public void setClientByClientId(ClientEntity clientByClientId) {
        this.clientByClientId = clientByClientId;
    }
}
