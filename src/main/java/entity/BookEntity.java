package entity;

import javax.persistence.*;
import java.sql.Date;
import java.util.Collection;
import java.util.Objects;

@Entity
@Table(name = "book", schema = "public", catalog = "db19_shcheden")
public class BookEntity {
    private int bookId;
    private String isbn;
    private String physicalSn;
    private String bookshelf;
    private Integer publisherId;
    private String bookName;
    private Date publishDt;
    private Collection<AuthorEntity> authors;
    private PublisherEntity publisherByPublisherId;
    private Collection<BorrowedBookEntity> borrowedBooksByBookId;

    @Id
    @Column(name = "book_id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int getBookId() {
        return bookId;
    }

    public void setBookId(int bookId) {
        this.bookId = bookId;
    }

    @Basic
    @Column(name = "isbn", nullable = true, length = 100)
    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    @Basic
    @Column(name = "physical_sn", nullable = true, length = 50)
    public String getPhysicalSn() {
        return physicalSn;
    }

    public void setPhysicalSn(String physicalSn) {
        this.physicalSn = physicalSn;
    }

    @Basic
    @Column(name = "bookshelf", nullable = true, length = 50)
    public String getBookshelf() {
        return bookshelf;
    }

    public void setBookshelf(String bookshelf) {
        this.bookshelf = bookshelf;
    }

    @Basic
    @Column(name = "publisher_id", nullable = true)
    public Integer getPublisherId() {
        return publisherId;
    }

    public void setPublisherId(Integer publisherId) {
        this.publisherId = publisherId;
    }

    @Basic
    @Column(name = "book_name", nullable = false, length = 100)
    public String getBookName() {
        return bookName;
    }

    public void setBookName(String bookName) {
        this.bookName = bookName;
    }

    @Basic
    @Column(name = "publish_dt", nullable = true)
    public Date getPublishDt() {
        return publishDt;
    }

    public void setPublishDt(Date publishDt) {
        this.publishDt = publishDt;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BookEntity that = (BookEntity) o;
        return bookId == that.bookId;
    }

    @Override
    public int hashCode() {
        return Objects.hash(bookId);
    }

//    @OneToMany(mappedBy = "bookByBookId")
//    public Collection<AuthorshipEntity> getAuthorshipsByBookId() {
//        return authorshipsByBookId;
//    }
//
//    public void setAuthorshipsByBookId(Collection<AuthorshipEntity> authorshipsByBookId) {
//        this.authorshipsByBookId = authorshipsByBookId;
//    }

    @ManyToMany(fetch = FetchType.LAZY) // here
    @JoinTable(name = "authorship",
            joinColumns = @JoinColumn(name = "book_id", referencedColumnName = "book_id"),
            inverseJoinColumns = @JoinColumn(name = "author_id", referencedColumnName = "author_id")
    )
    public Collection<AuthorEntity> getAuthors() {
        return authors;
    }

    public void setAuthors(Collection<AuthorEntity> authors) {
        this.authors = authors;
    }

    @ManyToOne(fetch = FetchType.LAZY) // here
    @JoinColumn(name = "publisher_id", referencedColumnName = "publisher_id", insertable = false, updatable = false)
    public PublisherEntity getPublisherByPublisherId() {
        return publisherByPublisherId;
    }

    public void setPublisherByPublisherId(PublisherEntity publisherByPublisherId) {
        this.publisherByPublisherId = publisherByPublisherId;
    }

    @OneToMany(mappedBy = "bookByBookId", fetch = FetchType.LAZY) // here
    public Collection<BorrowedBookEntity> getBorrowedBooksByBookId() {
        return borrowedBooksByBookId;
    }

    public void setBorrowedBooksByBookId(Collection<BorrowedBookEntity> borrowedBooksByBookId) {
        this.borrowedBooksByBookId = borrowedBooksByBookId;
    }

    @Override
    public String toString() {
        return "BookEntity{" +
                "bookId=" + bookId +
                ", isbn='" + isbn + '\'' +
                ", physicalSn='" + physicalSn + '\'' +
                ", bookshelf='" + bookshelf + '\'' +
                ", publisherId=" + publisherId +
                ", bookName='" + bookName + '\'' +
                ", publishDt=" + publishDt +
                ", publisherByPublisherId=" + publisherByPublisherId +
                ", borrowedBooksByBookId=" + borrowedBooksByBookId +
                '}';
    }
}
