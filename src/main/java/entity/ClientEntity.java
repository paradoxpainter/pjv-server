package entity;

import javax.persistence.*;
import java.sql.Date;
import java.util.Collection;
import java.util.Objects;

@Entity
@Table(name = "client", schema = "public", catalog = "db19_shcheden")
public class ClientEntity {
    private int clientId;
    private String phoneNumber;
    private Date birthDt;
    private Collection<BorrowedBookEntity> borrowedBooksByClientId;
    private PersonEntity personByClientId;
//    private Collection<ClientAddressEntity> clientAddressesByClientId;

    @Id
    @Column(name = "client_id", nullable = false)
    public int getClientId() {
        return clientId;
    }

    public void setClientId(int clientId) {
        this.clientId = clientId;
    }

    @Basic
    @Column(name = "phone_number", nullable = true, length = 50)
    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    @Basic
    @Column(name = "birth_dt", nullable = true)
    public Date getBirthDt() {
        return birthDt;
    }

    public void setBirthDt(Date birthDt) {
        this.birthDt = birthDt;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ClientEntity that = (ClientEntity) o;
        return clientId == that.clientId &&
                Objects.equals(phoneNumber, that.phoneNumber) &&
                Objects.equals(birthDt, that.birthDt);
    }

    @Override
    public int hashCode() {
        return Objects.hash(clientId, phoneNumber, birthDt);
    }

    @OneToMany(mappedBy = "clientByClientId", fetch = FetchType.LAZY) // here
    public Collection<BorrowedBookEntity> getBorrowedBooksByClientId() {
        return borrowedBooksByClientId;
    }

    public void setBorrowedBooksByClientId(Collection<BorrowedBookEntity> borrowedBooksByClientId) {
        this.borrowedBooksByClientId = borrowedBooksByClientId;
    }

    @OneToOne(fetch = FetchType.LAZY) // here
    @JoinColumn(name = "client_id", referencedColumnName = "person_id", nullable = false)
    public PersonEntity getPersonByClientId() {
        return personByClientId;
    }

    public void setPersonByClientId(PersonEntity personByClientId) {
        this.personByClientId = personByClientId;
    }

//    @OneToMany(mappedBy = "clientByClientId")
//    public Collection<ClientAddressEntity> getClientAddressesByClientId() {
//        return clientAddressesByClientId;
//    }

//    public void setClientAddressesByClientId(Collection<ClientAddressEntity> clientAddressesByClientId) {
//        this.clientAddressesByClientId = clientAddressesByClientId;
//    }
}
