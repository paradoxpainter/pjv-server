package entity;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "person", schema = "public", catalog = "db19_shcheden")
public class PersonEntity {
    private int personId;
    private String firstName;
    private String lastName;
    private String username;
    private String passwd;
    private String pRole;
    private AuthorEntity authorByPersonId;
    private ClientEntity clientByPersonId;
    private EmployeeEntity employeeByPersonId;

    @Id
    @Column(name = "person_id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int getPersonId() {
        return personId;
    }

    public void setPersonId(int personId) {
        this.personId = personId;
    }

    @Basic
    @Column(name = "first_name", nullable = false, length = 200)
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    @Basic
    @Column(name = "last_name", nullable = false, length = 200)
    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @Basic
    @Column(name = "username", nullable = true, length = 100)
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Basic
    @Column(name = "passwd", nullable = true, length = 100)
    public String getPasswd() {
        return passwd;
    }

    public void setPasswd(String passwd) {
        this.passwd = passwd;
    }

    @Basic
    @Column(name = "p_role", nullable = true, length = 50)
    public String getpRole() {
        return pRole;
    }

    public void setpRole(String pRole) {
        this.pRole = pRole;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PersonEntity that = (PersonEntity) o;
        return personId == that.personId &&
                Objects.equals(firstName, that.firstName) &&
                Objects.equals(lastName, that.lastName) &&
                Objects.equals(username, that.username) &&
                Objects.equals(passwd, that.passwd) &&
                Objects.equals(pRole, that.pRole);
    }

    @Override
    public int hashCode() {
        return Objects.hash(personId, firstName, lastName, username, passwd, pRole);
    }

    @OneToOne(mappedBy = "personByAuthorId")
    public AuthorEntity getAuthorByPersonId() {
        return authorByPersonId;
    }

    public void setAuthorByPersonId(AuthorEntity authorByPersonId) {
        this.authorByPersonId = authorByPersonId;
    }

    @OneToOne(mappedBy = "personByClientId")
    public ClientEntity getClientByPersonId() {
        return clientByPersonId;
    }

    public void setClientByPersonId(ClientEntity clientByPersonId) {
        this.clientByPersonId = clientByPersonId;
    }

    @OneToOne(mappedBy = "personByEmployeeId")
    public EmployeeEntity getEmployeeByPersonId() {
        return employeeByPersonId;
    }

    public void setEmployeeByPersonId(EmployeeEntity employeeByPersonId) {
        this.employeeByPersonId = employeeByPersonId;
    }
}
