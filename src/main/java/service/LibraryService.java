package service;

import helper.StopWatch;
import connection.SetupServer;
import entity.BookEntity;
import entity.BorrowedBookEntity;
import model.Book;
import repository.BookRepository;
import repository.BorrowRepository;
import repository.PersonRepository;
import utils.transform.BookEntityToBook;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import java.sql.Date;
import java.time.LocalDate;
import java.util.List;
import java.util.function.Supplier;
import java.util.logging.Logger;

public class LibraryService {
    private final static Logger LOGGER = Logger.getLogger(SetupServer.class.getName());

    private EntityManager EM = Persistence
            .createEntityManagerFactory("NewPersistenceUnit")
            .createEntityManager();

    private BookRepository bookRepository = new BookRepository(EM);
    private PersonRepository personRepository = new PersonRepository(EM);
    private BorrowRepository borrowRepository = new BorrowRepository(EM);

    public <R> R executeInTransaction(Supplier<R> runnable) {
        EntityTransaction transaction = EM.getTransaction();
        transaction.begin();

        R result = runnable.get();

        transaction.commit();
        return result;
    }

    public List<Book> getAll(Integer clientId) {
        StopWatch sw = new StopWatch();

        sw.start();
        EM.clear();
        sw.stop();
        LOGGER.info("---------------------------");
        LOGGER.info("EM.clear() " + sw.getElapsedTimeSecs());

        sw.start();
        List<BookEntity> b = executeInTransaction(() -> bookRepository.getAll());
        sw.stop();
        LOGGER.info("---------------------------");
        LOGGER.info("bookRepository.getAll " + sw.getElapsedTimeSecs());

        sw.start();
        List<Book> books = BookEntityToBook.entityToBook(b, clientId);
        sw.stop();
        LOGGER.info("---------------------------");
        LOGGER.info("BookEntityToBook.entityToBook " + sw.getElapsedTimeSecs());
        return books;
    }

    public boolean login(String username, String password) {
        return personRepository.isLoginDataCorrect(username, password);
    }

    public int getPersonById(String username) {
        return personRepository.getPersonIdByUsername(username);
    }

    public void borrowBook(Integer clientId, Integer bookId) {
        BorrowedBookEntity borrowedBookEntity = new BorrowedBookEntity();
        borrowedBookEntity.setClientId(clientId);
        borrowedBookEntity.setBookId(bookId);
        borrowedBookEntity.setBorrowDt(Date.valueOf(LocalDate.now()));
        borrowedBookEntity.setReturnDt(null);
        executeInTransaction(() -> {
            borrowRepository.create(borrowedBookEntity);
            return "";
        });
    }

    public void returnBook(int clientId, int bookId) {
        BorrowedBookEntity rbe = borrowRepository.getBorrowEntity(clientId, bookId);

        if (rbe != null) {
            try {
                executeInTransaction(() -> {
                    rbe.setReturnDt(Date.valueOf(LocalDate.now()));
                    borrowRepository.update(rbe);
                    return "";
                });

                LOGGER.info("SERVER: Client has returned the book " + bookId);
            } catch (Exception e) {
                LOGGER.severe("SERVER: Error while returning");
            }
        } else {
            LOGGER.warning("SERVER: The book is already returned");
        }
    }

    public BookRepository getBookRepository() {
        return bookRepository;
    }

    public PersonRepository getPersonRepository() {
        return personRepository;
    }

    public BorrowRepository getBorrowRepository() {
        return borrowRepository;
    }
}
