package utils.transform.abstraction;

import java.util.ArrayList;
import java.util.List;

public abstract class CollectionTransformer<E, F> {

    public abstract F transform(E e);

    /**
     * Transform List<E> to List<F>
     *
     * @param list input list
     * @return output list
     */
    public List<F> transform(List<E> list) {
        List<F> newList = new ArrayList<>();
        for (E e : list) {
            F test = transform(e);

            if (test != null) {
                newList.add(test);
            }
        }
        return newList;
    }
}
