package utils.transform;

import entity.BookEntity;
import entity.BorrowedBookEntity;
import model.Book;
import utils.transform.abstraction.CollectionTransformer;

import java.util.Collection;
import java.util.List;

public class BookEntityToBook {

    /**
     * Converting BookEntity list to list of Books.
     * Also assigning user who borrowed a book to it
     *
     * @param list   of BookEntities
     * @param userId user's id
     * @return
     */
    public static List<Book> entityToBook(List<BookEntity> list, int userId) {
        CollectionTransformer bookTransformer = new CollectionTransformer<BookEntity, Book>() {
            @Override
            public Book transform(BookEntity bookEntity) {
                Integer borrowedUserId = null;
                Collection<BorrowedBookEntity> c = bookEntity.getBorrowedBooksByBookId();

                if (c == null || c.size() == 0) {
                } else {
                    for (BorrowedBookEntity b : c) {
                        if (b.getReturnDt() == null) {
                            if (b.getClientId() == userId) {
                                borrowedUserId = userId;
                                break;
                            } else {
                                return null;
                            }
                        }
                    }
                }

                Book book = new Book(
                        bookEntity.getBookId(),
                        bookEntity.getBookName(),
                        bookEntity.getIsbn(),
                        bookEntity.getBookshelf(),
                        bookEntity.getPhysicalSn(),
                        bookEntity.getPublishDt(),
                        borrowedUserId
                );

                return book;
            }
        };

        return bookTransformer.transform(list);
    }

//    public static List<Book> entityToBook(List<BookEntity> list, int userId) {
//        List<Book> res = list.stream().map(x -> {
//            Optional<BorrowedBookEntity> bb = x.getBorrowedBooksByBookId().stream()
//                    .filter(y -> y.getReturnDt() == null && y.getClientId() == userId)
//                    .findAny();
//            Integer user = null;
//            if (bb.isPresent()) {
//                user = userId;
//            }
//
//            Book book = new Book(
//                    x.getBookId(),
//                    x.getBookName(),
//                    x.getIsbn(),
//                    x.getBookshelf(),
//                    x.getPhysicalSn(),
//                    x.getPublishDt(),
//                    user
//            );
//            return book;
//        }).collect(Collectors.toList());
//        return res;
//    }
}
