package model;

import java.io.Serializable;
import java.sql.Date;

public class Book implements Serializable {

    private static final long serialVersionUID = 1L;

    private int bookId;
    private String bookName;
    private String ISBN;
    private String bookShelf;
    private String serialNumber;
    private Date publishDate;
    private Integer borrowUserId;

    /**
     * Class is a prepared object for transfering data directly to the client.
     * All @params are fields of BookEntity
     *
     * @param bookId is artificial id
     * @param bookName is a name of book
     * @param ISBN ISBN
     * @param bookShelf shelf which book lays on
     * @param serialNumber magnet serial number
     * @param publishDate Date of publish
     * @param borrowUserId id of a Client who is reading the book at the moment
     */
    public Book(int bookId, String bookName, String ISBN,
                String bookShelf, String serialNumber, Date publishDate,
                Integer borrowUserId) {
        this.bookId = bookId;
        this.bookName = bookName;
        this.ISBN = ISBN;
        this.bookShelf = bookShelf;
        this.serialNumber = serialNumber;
        this.publishDate = publishDate;
        this.borrowUserId = borrowUserId;
    }

    public Integer getBorrowUserId() {
        return borrowUserId;
    }

    public void setBorrowUserId(Integer borrowUserId) {
        this.borrowUserId = borrowUserId;
    }

    public int getBookId() {
        return bookId;
    }

    public void setBookId(int bookId) {
        this.bookId = bookId;
    }

    public String getBookName() {
        return bookName;
    }

    public void setBookName(String bookName) {
        this.bookName = bookName;
    }

    public String getISBN() {
        return ISBN;
    }

    public void setISBN(String ISBN) {
        this.ISBN = ISBN;
    }

    public String getBookShelf() {
        return bookShelf;
    }

    public void setBookShelf(String bookShelf) {
        this.bookShelf = bookShelf;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public Date getPublishDate() {
        return publishDate;
    }

    public void setPublishDate(Date publishDate) {
        this.publishDate = publishDate;
    }
}
