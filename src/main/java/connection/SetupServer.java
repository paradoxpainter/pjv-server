package connection;

import handler.BookHandler;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.logging.Logger;

public class SetupServer {

    private final static Logger LOGGER = Logger.getLogger(SetupServer.class.getName());

    private ServerSocket server;

    /**
     * Creating socket on port 65100 by default
     *
     * @throws IOException
     */
    public void serverSetup() throws IOException {
        server = new ServerSocket(65100);
        LOGGER.info("SERVER: Server has started working.");
    }

    /**
     * Shutting down the server
     *
     * @throws IOException
     */
    public void shutDownServer() throws IOException {
        server.close();
    }

    /**
     * Server's job list. Server's logic is here.
     *
     * @throws IOException
     */
    public void run() throws IOException {
        serverSetup();

        while (true) {
            LOGGER.info("SERVER: Listening..");
            Socket connectionToClient = server.accept();

            Thread thread = new BookHandler(connectionToClient);
            thread.start();

            LOGGER.info("SERVER: Client was successfully disconnected\n");
        }
    }

    public ServerSocket getServer() {
        return server;
    }
}
