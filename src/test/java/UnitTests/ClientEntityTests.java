package UnitTests;

import entity.BorrowedBookEntity;
import entity.ClientEntity;
import entity.PersonEntity;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.sql.Date;
import java.util.Collection;

import static Data.DataGenerator.*;
import static org.mockito.Mockito.mock;

public class ClientEntityTests {
    @Test
    public void SettersTest(){
        // Arrange
        ClientEntity sut = new ClientEntity();

        // Act
        int expectedId = GenerateInt();
        sut.setClientId(expectedId);

        String expectedPhoneNumber = GenerateString(7);
        sut.setPhoneNumber(expectedPhoneNumber);

        Date expectedDate = new Date(GenerateLong());
        sut.setBirthDt(expectedDate);

        PersonEntity personEntity = mock(PersonEntity.class);
        sut.setPersonByClientId(personEntity);

        Collection<BorrowedBookEntity> borrowedBooks = mock(Collection.class);
        sut.setBorrowedBooksByClientId(borrowedBooks);

        // Assert
        Assertions.assertEquals(expectedId, sut.getClientId());
        Assertions.assertEquals(expectedPhoneNumber, sut.getPhoneNumber());
        Assertions.assertEquals(expectedDate, sut.getBirthDt());
        Assertions.assertEquals(personEntity, sut.getPersonByClientId());
        Assertions.assertEquals(borrowedBooks, sut.getBorrowedBooksByClientId());
    }
}
