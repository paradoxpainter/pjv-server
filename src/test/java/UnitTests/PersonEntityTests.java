package UnitTests;

import entity.AuthorEntity;
import entity.ClientEntity;
import entity.PersonEntity;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static Data.DataGenerator.GenerateInt;
import static Data.DataGenerator.GenerateString;
import static org.mockito.Mockito.mock;

public class PersonEntityTests {
    @Test
    public void SettersTest(){
        // Arrange
        PersonEntity sut = new PersonEntity();

        // Act
        String expectedFirstName = GenerateString(7);
        sut.setFirstName(expectedFirstName);

        String expectedLastName = GenerateString(7);
        sut.setLastName(expectedLastName);

        String expectedUserName = GenerateString(7);
        sut.setUsername(expectedUserName);

        String expectedPassword = GenerateString(7);
        sut.setPasswd(expectedPassword);

        int expectedId = GenerateInt();
        sut.setPersonId(expectedId);

        String expectedRole = GenerateString(7);
        sut.setpRole(expectedRole);

        AuthorEntity authorEntity = mock(AuthorEntity.class);
        sut.setAuthorByPersonId(authorEntity);

        ClientEntity clientEntity = mock(ClientEntity.class);
        sut.setClientByPersonId(clientEntity);

        // Assert
        Assertions.assertEquals(sut.getFirstName(), expectedFirstName);
        Assertions.assertEquals(sut.getLastName(), expectedLastName);
        Assertions.assertEquals(sut.getUsername(), expectedUserName);
        Assertions.assertEquals(sut.getPasswd(), expectedPassword);
        Assertions.assertEquals(sut.getPersonId(), expectedId);
        Assertions.assertEquals(sut.getpRole(), expectedRole);
        Assertions.assertEquals(sut.getAuthorByPersonId(), authorEntity);
        Assertions.assertEquals(sut.getClientByPersonId(), clientEntity);
    }

    @Test
    public void SettersReassignTest(){
        // Arrange
        PersonEntity sut = new PersonEntity();

        sut.setFirstName(GenerateString(7));
        sut.setLastName(GenerateString(7));
        sut.setUsername(GenerateString(7));
        sut.setPasswd(GenerateString(7));
        sut.setPersonId(GenerateInt());
        sut.setpRole(GenerateString(7));
        sut.setAuthorByPersonId(mock(AuthorEntity.class));
        sut.setClientByPersonId(mock(ClientEntity.class));

        // Act
        String expectedFirstName = GenerateString(7);
        sut.setFirstName(expectedFirstName);

        String expectedLastName = GenerateString(7);
        sut.setLastName(expectedLastName);

        String expectedUserName = GenerateString(7);
        sut.setUsername(expectedUserName);

        String expectedPassword = GenerateString(7);
        sut.setPasswd(expectedPassword);

        int expectedId = GenerateInt();
        sut.setPersonId(expectedId);

        String expectedRole = GenerateString(7);
        sut.setpRole(expectedRole);

        AuthorEntity authorEntity = mock(AuthorEntity.class);
        sut.setAuthorByPersonId(authorEntity);

        ClientEntity clientEntity = mock(ClientEntity.class);
        sut.setClientByPersonId(clientEntity);

        // Assert
        Assertions.assertEquals(sut.getFirstName(), expectedFirstName);
        Assertions.assertEquals(sut.getLastName(), expectedLastName);
        Assertions.assertEquals(sut.getUsername(), expectedUserName);
        Assertions.assertEquals(sut.getPasswd(), expectedPassword);
        Assertions.assertEquals(sut.getPersonId(), expectedId);
        Assertions.assertEquals(sut.getpRole(), expectedRole);
        Assertions.assertEquals(sut.getAuthorByPersonId(), authorEntity);
        Assertions.assertEquals(sut.getClientByPersonId(), clientEntity);
    }
}
