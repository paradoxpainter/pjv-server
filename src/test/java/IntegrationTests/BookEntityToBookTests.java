package IntegrationTests;

import entity.BookEntity;
import model.Book;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import utils.transform.BookEntityToBook;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import static Data.DataGenerator.*;

public class BookEntityToBookTests {
    @Test
    public void entityToBookTest(){
        // Arrange
        BookEntity bookEntity = new BookEntity();

        int expectedBookId = GenerateInt();
        bookEntity.setBookId(expectedBookId);

        String expectedName = GenerateString(7);
        bookEntity.setBookName(expectedName);

        String expectedShelf = GenerateString(7);
        bookEntity.setBookshelf(expectedShelf);

        String expectedISBN = GenerateString(7);
        bookEntity.setIsbn(expectedISBN);

        String expectedNumber = GenerateString(7);
        bookEntity.setPhysicalSn(expectedNumber);

        Date expectedDate = new Date(GenerateLong());
        bookEntity.setPublishDt(expectedDate);

        List<BookEntity> list = new ArrayList<>();
        list.add(bookEntity);

        // Act
        List<Book> books = BookEntityToBook.entityToBook(list, GenerateInt());
        Book book = books.get(0);

        // Assert
        Assertions.assertEquals(expectedBookId, book.getBookId());
        Assertions.assertEquals(expectedName, book.getBookName());
        Assertions.assertEquals(expectedISBN, book.getISBN());
        Assertions.assertEquals(expectedShelf, book.getBookShelf());
        Assertions.assertEquals(expectedNumber, book.getSerialNumber());
        Assertions.assertEquals(expectedDate, book.getPublishDate());
    }

    @Test
    public void entityToBookReassignTest(){
        // Arrange
        BookEntity bookEntity = new BookEntity();
        bookEntity.setBookId(GenerateInt());
        bookEntity.setBookName(GenerateString(7));
        bookEntity.setBookshelf(GenerateString(7));
        bookEntity.setIsbn(GenerateString(7));
        bookEntity.setPhysicalSn(GenerateString(7));
        bookEntity.setPublishDt(new Date(GenerateLong()));

        // Act
        int expectedBookId = GenerateInt();
        bookEntity.setBookId(expectedBookId);

        String expectedName = GenerateString(7);
        bookEntity.setBookName(expectedName);

        String expectedShelf = GenerateString(7);
        bookEntity.setBookshelf(expectedShelf);

        String expectedISBN = GenerateString(7);
        bookEntity.setIsbn(expectedISBN);

        String expectedNumber = GenerateString(7);
        bookEntity.setPhysicalSn(expectedNumber);

        Date expectedDate = new Date(GenerateLong());
        bookEntity.setPublishDt(expectedDate);

        List<BookEntity> list = new ArrayList<>();
        list.add(bookEntity);

        List<Book> books = BookEntityToBook.entityToBook(list, GenerateInt());
        Book book = books.get(0);

        // Assert
        Assertions.assertEquals(expectedBookId, book.getBookId());
        Assertions.assertEquals(expectedName, book.getBookName());
        Assertions.assertEquals(expectedISBN, book.getISBN());
        Assertions.assertEquals(expectedShelf, book.getBookShelf());
        Assertions.assertEquals(expectedNumber, book.getSerialNumber());
        Assertions.assertEquals(expectedDate, book.getPublishDate());
    }


}
