package IntegrationTests;

import entity.AuthorEntity;
import entity.BookEntity;
import entity.PersonEntity;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static Data.DataGenerator.*;

public class AuthorEntityTests {
    @Test
    public void SetBooksTest(){
        // Arrange
        AuthorEntity authorEntity = new AuthorEntity();

        BookEntity bookEntity = new BookEntity();

        int expectedBookId = GenerateInt();
        bookEntity.setBookId(expectedBookId);

        String expectedName = GenerateString(7);
        bookEntity.setBookName(expectedName);

        String expectedShelf = GenerateString(7);
        bookEntity.setBookshelf(expectedShelf);

        String expectedISBN = GenerateString(7);
        bookEntity.setIsbn(expectedISBN);

        String expectedNumber = GenerateString(7);
        bookEntity.setPhysicalSn(expectedNumber);

        Date expectedDate = new Date(GenerateLong());
        bookEntity.setPublishDt(expectedDate);

        List<BookEntity> expectedBooks = new ArrayList<>();
        expectedBooks.add(bookEntity);
        authorEntity.setBooks(expectedBooks);

        // Act
        Collection<BookEntity> actualBooks = authorEntity.getBooks();

        // Assert
        Assertions.assertEquals(expectedBooks, actualBooks);
    }

    @Test
    public void Test(){
        // Arrange
        AuthorEntity authorEntity = new AuthorEntity();

        PersonEntity personEntity = new PersonEntity();
        authorEntity.setPersonByAuthorId(personEntity);

        // Act
        PersonEntity result = authorEntity.getPersonByAuthorId();

        // Assert
        Assertions.assertEquals(personEntity, result);
    }
}
