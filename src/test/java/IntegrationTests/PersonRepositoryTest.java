package IntegrationTests;

import entity.PersonEntity;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import service.LibraryService;

import static org.junit.jupiter.api.Assertions.*;
import static Data.DataGenerator.*;
import static org.mockito.Mockito.*;

class PersonRepositoryTest {

    LibraryService libraryService = new LibraryService();

    @Test
    void PersonRepositoryCRUDTest() {
        // Arrange
        PersonEntity testEntity = new PersonEntity();

        testEntity.setFirstName(GenerateString(10));
        testEntity.setLastName(GenerateString(10));

        // Act
        PersonEntity expectedEntity = libraryService.executeInTransaction(() -> {
            int personId = libraryService.getPersonRepository().create(testEntity).getPersonId();
            PersonEntity tmp = libraryService.getPersonRepository().findOne(personId);
            tmp.setLastName(GenerateString(5));
            libraryService.getPersonRepository().update(tmp);
            libraryService.getPersonRepository().delete(tmp);

            return tmp;
        });

        // Assert
        assertEquals(testEntity.getPersonId(), expectedEntity.getPersonId());
        assertEquals(testEntity.getFirstName(), expectedEntity.getFirstName());
        assertEquals(testEntity.getLastName(), expectedEntity.getLastName());
    }

    @Test
    void GetPswdByUsernameNullTest() {
        // Arrange
        String username = GenerateString(12);

        // Act
        String password = libraryService.getPersonRepository().getPswdByUsername(username);

        // Assert
        assertNull(password);
    }

    @Test
    void IsLoginDataCorrectTest() {
        // Arrange
        String username = GenerateString(12);
        String password = GenerateString(15);

        // Act
        boolean isCorrect = libraryService.getPersonRepository().isLoginDataCorrect(username, password);

        // Assert
        assertFalse(isCorrect);
    }

    @Test
    void GetPersonIdByUsername() {
        // Arrange
        String username = GenerateString(12);

        // Act
        int userId = libraryService.getPersonRepository().getPersonIdByUsername(username);

        // Assert
        assertEquals(-1, userId);
    }
}